% This is the code to the paper:  
Jingwei Song, Shaobo Xia, Jun Wang, Mitesh Patel, Dong Chen, "Uncertainty Quantification of Hyperspectral Image Denoising Frameworks based on Sliding-Window Low-Rank Matrix Approximation,”IEEE TRANSACTIONS ON GEOSCIENCE AND REMOTE SENSING (Accepted)  https://arxiv.org/abs/2004.10959    
% The code was conducted in reference to the code provided by:  
H. Zhang, W. He, L. Zhang, H. Shen, and Q. Yuan, “Hyperspectral image restoration using low-rank matrix recovery,” IEEE Transactions on Geoscience and Remote Sensing, vol. 52, no. 8, pp. 4729–4743, 2013.  
% 
% 1.Run "Main_uncertainty.m",  
% which will run the LRMR algorithm in the Monte-Carlo manner.  
% 2.Run "Main_uncertainty_postprocess.m",  
% which will calcualte the coverage rate of the proposed closed-form  
% uncertainty estimatation code.  
% Note that the Monte Carlo tests are very slow, please be patient.  
%  
%  
% Author: Jingwei Song (May, 2020)  
% E-mail addresses:(jingweisong.eng@gmail.com)  
