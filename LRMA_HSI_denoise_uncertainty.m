function [ output_image,var0_image ] = LRMR_HSI_denoise_uncertainty( oriData3_noise,r,M,s,stepszie,var0 )

% NOTE:  LRMR is implemented with ssGoDec, which is different from the one 
%        mentioned in the original paper [1]. The selection of parameter s is 
%        changed correspondingly.


%  oriData3_noise                            noisy 3-D image normalized to [0,1] band by band
%  r (recommended value 7)                   the rank of each sub-cub(matrix version)
%  M (recommended value20)                   the spatial size of each sub cube (line*column=M*M)
%  s (recommended value 7000 for GoDec)      the number of pixels which are corrupted by sparse noise
%  s (recommended value q% for ssGoDec)      q% is the percentage of pixels which are corrupted by sparse noise
%  stepszie (recommended value 4)            stepsize of each sub-cub
%   Jingwei
%   Std:                                    Standard deviation of the image
%%%%%%%%%%%%%%%%  
 [m,n,p] = size(oriData3_noise);
 clear_image=zeros(m,n,p);
 var0_image=zeros(m,n,p);
 
 
%  Sparse=zeros(m,n,p);            
  
 Weight = zeros(m,n,p);      % weight matrix 
 patch_block = zeros(M^2,p); % the size of each block
%  sparse_block=zeros(M^2,p);         
R         =   m-M+1;
C         =   n-M+1;
rr        =   [1:stepszie:R];
% rr        =   [1:stepszie:m];
rr        =   [rr rr(end)+1:R];   %   Seems to be the error
cc        =   [1:stepszie:C];
% cc        =   [1:stepszie:n];
cc        =   [cc cc(end)+1:C];   %   Seems to be the error
row       =   length(rr);
column    =   length(cc);


for   rownumber =1:row
     for columnnumber = 1:column
         i = rr(rownumber);
         j = cc(columnnumber); 
        for  k=1:1:p                     
         Weight(i:1:i+M-1,j:1:j+M-1,k) = Weight(i:1:i+M-1,j:1:j+M-1,k)+1;               
        end  
     end
end  

 Weight_last = 1./Weight;
%  Weight_inv_sqr = 1./(Weight.^2); %   MOdify by Jingwei, the variance
%  is independent because the obervation is fixed (only once)
 Weight_inv_sqr = 1./(Weight);
 
 %  Step I. The pixel to block matrix. Add by Jingwei. 
 table_x = zeros(m*n,1);    %   Index of the block in X
 table_y = zeros(m*n,1);    %   Index of the block in Y
 table_locx = zeros(m*n,1); %   Index of the location in this block X
 table_locy = zeros(m*n,1); %   Index of the location in this block Y
 blk_ind = 0;
 for   rownumber =1:row
     for columnnumber = 1:column
         blk_ind = blk_ind + 1;
         % Iteration
         for i = rr(rownumber) : rr(rownumber)+M-1
             for j = cc(columnnumber) : cc(columnnumber)+M-1
                 flag = false;
                 ii = 1;
                 while(ii<=size(table_x,2))
                     if(table_x((i-1)*n+j,ii)==0)
                         table_x((i-1)*n+j,ii) = rownumber;
                         table_y((i-1)*n+j,ii) = columnnumber;
                         table_locx((i-1)*n+j,ii) = i-rr(rownumber)+1;
                         table_locy((i-1)*n+j,ii) = j-cc(columnnumber)+1;
                         flag = true;
                         break;
                     end
                     ii = ii + 1;
                 end
                 if(flag==false)
                     table_x((i-1)*n+j,ii) = rownumber;
                     table_y((i-1)*n+j,ii) = columnnumber;
                     table_locx((i-1)*n+j,ii) = i-rr(rownumber)+1;
                     table_locy((i-1)*n+j,ii) = j-cc(columnnumber)+1;
                 end
             end
         end
     end
 end
 %  End Step I
 
 %  Step II: Calculate each block and store them individually
 block = cell(row,column);
 for   rownumber =1:row
     for columnnumber = 1:column
         i = rr(rownumber);
         j = cc(columnnumber); 
        for  k=1:1:p                     
         patch_reference = oriData3_noise(i:i+M-1,j:j+M-1,k); 
         patch_block(:,k) =  patch_reference(:);             
        end
%        [clear_patch_block,S,~,~]=GoDec( patch_block,r,s,0);   % used in the original paper
         [clear_patch_block,S,~,~]=SSGoDec( patch_block,r,s,0); % 
         tmp = zeros(M,M,p);
         for m2=1:1:p
             clear_image(i:1:i+M-1,j:1:j+M-1,m2) = reshape(clear_patch_block(:,m2),M,M)+clear_image(i:1:i+M-1,j:1:j+M-1,m2);
             %         Sparse(i:1:i+M-1,j:1:j+M-1,m2) = reshape(S(:,m2),M,M)+Sparse(i:1:i+M-1,j:1:j+M-1,m2);                             %ϡ����
             % ============ Uncertainty estimation Jingwei 16 Jan ================== % 
             std_patch_block = zeros(size(clear_patch_block));
             [X,Y,U,Sigma,V]= svdXY_rank(clear_patch_block,r);
             tmpX = X*inv(X'*X)*X';
             tmpY = Y*inv(Y'*Y)*Y';
             for mm = 1 : size(clear_patch_block,1)
                 for nn = 1 : size(clear_patch_block,2)
                     std_patch_block(mm,nn) = var0*(tmpX(mm,mm) + tmpY(nn,nn));
                 end
             end
             tmp(:,:,m2) = reshape(std_patch_block(:,m2),M,M);
             % ======================== end ============================== %
         end 
         block{rownumber,columnnumber} = tmp;
     end
 end
 %  End of Step II.
 
 %  Step III: Estimate the uncertainty based on the block
 %  (a) Calculate eta_table
 % a1: upper-left
 eta_table11 = zeros(size(table_x,2));
 for ii = 1 : ceil(M/stepszie)
     for jj = 1 : ceil(M/stepszie)
         eta_table11(ii,jj) = ((M-(ii-1)*stepszie)*(M-(jj-1)*stepszie))/(M*M);
     end
 end
  % a2: upper-right
 eta_table12 = zeros(size(table_x,2));
 for ii = 1 : ceil(M/stepszie)
     for jj = 1 : mod(C,stepszie)
         eta_table12(ii,jj) = ((M-(ii-1)*stepszie)*(M-(jj-1)*1))/(M*M);
     end
 end
  % a3: bottom-left
 eta_table21 = zeros(size(table_x,2));
 for ii = 1 : mod(R,stepszie)
     for jj = 1 : ceil(M/stepszie)
         eta_table21(ii,jj) = ((M-(ii-1)*1)*(M-(jj-1)*stepszie))/(M*M);
     end
 end
 % a4: bottom-left
 eta_table22 = zeros(size(table_x,2));
 for ii = 1 : mod(R,stepszie)
     for jj = 1 : mod(C,stepszie)
         eta_table22(ii,jj) = ((M-(ii-1)*1)*(M-(jj-1)*1))/(M*M);
     end
 end
 %  (b) Quantify uncertainty based on block and eta
 %  \sum_{i=1}^N\frac{1}{N^2}\sigma_i^2+\sum_{i=1}^K2*(\frac{1}{N})^2*\eta_{ij}\sigma_1\sigma_2
 loc_thresholdX = floor(R/stepszie)+1;
 loc_thresholdY = floor(C/stepszie)+1;
 for i = 1 : m
     for j = 1 : n
         %  Calculating the uncertainty
         num_element = sum(table_x((i-1)*n+j,:)>0);
         tmp = zeros(1,1,p);
         for ii = 1 : num_element
             for jj = ii: num_element
                 blk_ind1_x = table_x((i-1)*n+j,ii);locx_1 = table_locx((i-1)*n+j,ii);
                 blk_ind1_y = table_y((i-1)*n+j,ii);locy_1 = table_locy((i-1)*n+j,ii);
                 blk_ind2_x = table_x((i-1)*n+j,jj);locx_2 = table_locx((i-1)*n+j,jj);
                 blk_ind2_y = table_y((i-1)*n+j,jj);locy_2 = table_locy((i-1)*n+j,jj);
                 if(ii == jj)
                     tmp = tmp + (1/num_element^2)*block{blk_ind1_x,blk_ind1_y}(locx_1,locy_1,:);
                 else
                     if(blk_ind2_x<=loc_thresholdX&&blk_ind2_y<=loc_thresholdY)
                         eta = eta_table11(abs(blk_ind2_x-blk_ind1_x)+1,abs(blk_ind2_y-blk_ind1_y)+1);
                     end
                     if(blk_ind2_x<=loc_thresholdX&&blk_ind2_y>loc_thresholdY)
                         eta = eta_table12(abs(blk_ind2_x-blk_ind1_x)+1,abs(blk_ind2_y-blk_ind1_y)+1);
                     end
                     if(blk_ind2_x>loc_thresholdX&&blk_ind2_y<=loc_thresholdY)
                         eta = eta_table21(abs(blk_ind2_x-blk_ind1_x)+1,abs(blk_ind2_y-blk_ind1_y)+1);
                     end
                     if(blk_ind2_x>loc_thresholdX&&blk_ind2_y>loc_thresholdY)
                         eta = eta_table22(abs(blk_ind2_x-blk_ind1_x)+1,abs(blk_ind2_y-blk_ind1_y)+1);
                     end
                     if(eta > 0)
                     tmp = tmp + 2*(1/num_element^2)*sqrt(block{blk_ind1_x,blk_ind1_y}(locx_1,locy_1,:)).*...
                         sqrt(block{blk_ind2_x,blk_ind2_y}(locx_2,locy_2,:))*eta;
                     end
                 end
             end
         end
         var0_image(i,j,:) = tmp;
     end
 end
 
 
 

 output_image = Weight_last.*clear_image;

end
%%
function [X,Y,U,Sigma,V]= svdXY_rank( S ,rank)

[U,Sigma,V] = svd(S);

%   Shrink U Sigma V by rank
tmp = diag(Sigma);
U = U(:,1:rank);
Sigma = diag(tmp(1:rank));
V = V(:,1:rank);

[m,n] = size(Sigma);
Sigma_sqrt = sqrt(Sigma);
if(m >= n)
    X = U*Sigma_sqrt;
    Y = Sigma_sqrt(1:n,1:n)*V';
else 
    X = U*Sigma_sqrt(1:m,1:m);
    Y = Sigma_sqrt*V';
end
Y = Y';
end

%%
function[L,S,RMSE,error]=SSGoDec(X,rank,card,power)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      Semi-Soft GoDec Algotithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INPUTS:
%X: nxp data matrix with n samples and p features
%rank: rank(L)<=rank
%card: card(S)<=card
%power: >=0, power scheme modification, increasing it lead to better
%accuracy and more time cost
%OUTPUTS:
%L:Low-rank part
%S:Sparse part
%RMSE: error
%error: ||X-L-S||/||X||
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%REFERENCE:
%Tianyi Zhou and Dacheng Tao, "GoDec: Randomized Lo-rank & Sparse Matrix
%Decomposition in Noisy Case", ICML 2011
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Tianyi Zhou, 2011, All rights reserved.

%iteration parameters
iter_max=1e+2;
error_bound=1e-10;
iter=1;
RMSE=[];

%matrix size
[m,n]=size(X);
if m<n X=X'; end

%initialization of L and S
L=X;
S=sparse(zeros(size(X)));

% tic;
while true

    %Update of L
    Y2=randn(n,rank);
    for i=1:power+1
        Y1=L*Y2;
        Y2=L'*Y1;
    end
    [Q,R]=qr(Y2,0);
    L_new=(L*Q)*Q';
    
    %Update of S
    T=L-L_new+S;
    L=L_new;
    S=wthresh(T,'s',card);
%     [~,idx]=sort(abs(T(:)),'descend');
%     S=zeros(size(X));S(idx(1:card))=T(idx(1:card));
    
    %Error, stopping criteria
    %T(idx(1:card))=0;
    T=T-S;
    RMSE=[RMSE norm(T(:))];
    if RMSE(end)<error_bound || iter>iter_max
        break;
    else
        L=L+T;
    end
    iter=iter+1;
    
end
% toc;

LS=L+S;
error=norm(LS(:)-X(:))/norm(X(:));
if m<n 
    LS=LS';
    L=L';
    S=S';
end
end