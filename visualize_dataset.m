clear all;close all;clc;
load DCClip.mat;
HSI_image = cD;
% load simu_indian.mat;
% HSI_image = simu_indian;

ind_red = 60;
ind_green = 27;
ind_blue = 17;


image = zeros(size(HSI_image,1),size(HSI_image,2),3);
image(:,:,1) = 255 * HSI_image(:,:,ind_red) / max(max(HSI_image(:,:,ind_red)));
image(:,:,2) = 255 * HSI_image(:,:,ind_green) /max(max(HSI_image(:,:,ind_green)));
image(:,:,3) = 255 * HSI_image(:,:,ind_blue) /max(max(HSI_image(:,:,ind_blue)));

image = uint8(image);
imshow(image);