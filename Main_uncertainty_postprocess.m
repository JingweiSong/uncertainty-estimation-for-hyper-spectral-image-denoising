clc;clear all;close all;

%   Parameter control
load('simu_indian.mat');
load(['Simulationdataset_info','.mat']);
ind_x = [1 3 5 30];  %   For drawing Q-Q map
ind_y = [1 4 8 30];  %   For drawing Q-Q map
ind_z = [1 4 8 30];  %   For drawing  Q-Q map
NUM_DATASET = 1;
result_coverage = zeros(NUM_DATASET,2);  % mean + std0
result_swtest = zeros(NUM_DATASET,1);  % mean + std0




for i_data = 1 : NUM_DATASET
    %   Load data
    for n = 1 : batch_iter
        load(['Simulationdataset_',num2str(1000*std0),'_',num2str(n),'.mat']);
        output_image{n} = output_image0;
        var0_image{n} = var0_image0;
    end
    
    %   End of Load data
    
%     r = round(r - 0.2*r); %   Modified by Jingwei. For robustness test, please comment.
    element_xyz = zeros(batch_iter,1);
    %   Generate post statistics
    output_image_mean = zeros(size(output_image{1}));
    for iter = 1 : batch_iter
        output_image_mean = output_image_mean + output_image{1,iter};
    end
    output_image_mean = output_image_mean / batch_iter;
    
    [M N p] = size(OriData3);
    rate_original = zeros(M,N,p);
    Var_Wxx = zeros(M,N,p);
    for iter = 1 : batch_iter
        output_image_subspace = output_image{1,iter};
        err3D_original_post = output_image_subspace - output_image_mean;
        for i1 = 1 : M
            for i2 = 1 : N
                for i3 = 1 : p
                    if(abs(err3D_original_post(i1,i2,i3))<1.96*sqrt(var0_image{1,iter}(i1,i2,i3)))
                        rate_original(i1,i2,i3) = rate_original(i1,i2,i3) + 1;
                    end
                    if(i1 == ind_x(i_data)&& i2 == ind_y(i_data)&& i3 == ind_z(i_data))
                        element_xyz(iter) = output_image{1,iter}(i1,i2,i3);
                    end
                end
            end
        end
    end
    rate_original = rate_original / batch_iter;
    mean_rate_original = mean(mean(mean(rate_original)));
    std0_rate_original = std(rate_original(:));
    result_coverage(i_data,:) = [mean_rate_original std0_rate_original];
%     mean_rate_yuchao = mean(mean(rate_yuchao));
    figure(i_data);
    qqplot(element_xyz);grid on;
    set(gcf,'color','w');
    [H,result_swtest(i_data)] = swtest(element_xyz);
end
disp('The results are shown in "mean", "std0"');
result_coverage
result_swtest


function [X,Y,U,Sigma,V]= svdXY_threshold( S ,threshold_r)

[U,Sigma,V] = svd(S);

%   Shrink U Sigma V by r
tmp = diag(Sigma);
r = sum(tmp > threshold_r);
U = U(:,1:r);
Sigma = diag(tmp(1:r));
V = V(:,1:r);

[m,n] = size(Sigma);
Sigma_sqrt = sqrt(Sigma);
if(m >= n)
    X = U*Sigma_sqrt;
    Y = Sigma_sqrt(1:n,1:n)*V';
else 
    X = U*Sigma_sqrt(1:m,1:m);
    Y = Sigma_sqrt*V';
end
Y = Y';
end

function [X,Y,U,Sigma,V]= svdXY_r( S ,r)

[U,Sigma,V] = svd(S);

%   Shrink U Sigma V by r
tmp = diag(Sigma);
U = U(:,1:r);
Sigma = diag(tmp(1:r));
V = V(:,1:r);

[m,n] = size(Sigma);
Sigma_sqrt = sqrt(Sigma);
if(m >= n)
    X = U*Sigma_sqrt;
    Y = Sigma_sqrt(1:n,1:n)*V';
else 
    X = U*Sigma_sqrt(1:m,1:m);
    Y = Sigma_sqrt*V';
end
Y = Y';
end

function [S_subspace]= project( S ,r)

[U,Sigma,V] = svd(S);

%   Shrink U Sigma V by r
tmp = diag(Sigma);
U = U(:,1:r);
Sigma = diag(tmp(1:r));
V = V(:,1:r);


S_subspace = U*Sigma*V';

end