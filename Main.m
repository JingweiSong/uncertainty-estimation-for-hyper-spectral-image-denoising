% clear all;close all;clc;
%===============  Parameter setting  ====================%
batch_iter = 10;     %   Number of Monte Carlo tests -- 
                    %   Consider increasing the batch for more accuracy
std0 = 0.05;       %   Standard deviation of the noise
load 'simu_indian'  %   Dataset
r = 7;              %   Rank
blocksize =20;      %   Block size
ratio_randomnoise=0;%   Ratio of random noise	
stepsize = 10;
%========================================================%


var0 = std0^2;
s = 0.15;           %   GoDec parameter
disp('Please be patient, the Monte Carlo tests is slow');
parfor iter = 1 : batch_iter
    disp(['Monte Carlo test:' num2str(iter)]);
    [M N p] = size(OriData3);
    oriData3_noise = OriData3;

    ratio = ratio_randomnoise*ones(1,p);           
    noiselevel = std0*ones(1,p);      
    %noise simulated
    for i =1:p
        oriData3_noise(:,:,i)=OriData3(:,:,i)  + noiselevel(i)*randn(M,N);
    end

    for i =1:p
        oriData3_noise(:,:,i)=imnoise(oriData3_noise(:,:,i),'salt & pepper',ratio(i));
    end
    
    
    [ output_image{iter},var0_image{iter} ] = LRMR_HSI_denoise_uncertainty( oriData3_noise,r,blocksize,s,stepsize,var0 );
  
end
save(['Simulationdataset_info','.mat'],'r','batch_iter','std0');
for i = 1 : batch_iter
    output_image0 = output_image{i};
    var0_image0 = var0_image{i};
    save(['Simulationdataset_',num2str(1000*std0),'_',num2str(i),'.mat'],'output_image0','var0_image0');
end

